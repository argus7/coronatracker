//
//  FaqCell.swift
//  CoronaTracker
//
//  Created by Ravi Ranjan on 04/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import UIKit

class FaqCell: UITableViewCell {

    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
