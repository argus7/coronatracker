//
//  stateListCell.swift
//  CoronaTracker
//
//  Created by Ravi Ranjan on 01/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import UIKit

class stateListCell: UITableViewCell {

    @IBOutlet weak var deathLabel: UILabel!
    @IBOutlet weak var recoveredlabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var stateNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
