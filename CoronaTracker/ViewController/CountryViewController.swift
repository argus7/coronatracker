//
//  CountryViewController.swift
//  CoronaTracker
//
//  Created by Ravi Ranjan on 03/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import UIKit
import SwiftyJSON
import ADCountryPicker
class CountryViewController: UIViewController , ADCountryPickerDelegate{
    
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var totalCaseMainLabel: UILabel!
    @IBOutlet weak var totalCaseMainView: UIView!
    @IBOutlet weak var overallStatsView: UIView!
    
    @IBOutlet weak var casePerMillionLabel: UILabel!
    @IBOutlet weak var todayStatsView: UIView!
    
    @IBOutlet weak var todayDeathlabel: UILabel!
    var picker = ADCountryPicker()
    
    var countryByData : CCData?
    
    @IBOutlet weak var deathPerMillionLabel: UILabel!
    @IBOutlet weak var todayTotalCase: UILabel!
    @IBOutlet weak var overallTotalCaseLabel: UILabel!
    
    @IBOutlet weak var overAllDeathLabel: UILabel!
    @IBOutlet weak var overAllActiveCaseLabel: UILabel!
    @IBOutlet weak var overallRecoveredCaseLabel: UILabel!
    @IBOutlet weak var selectCountryView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Country Dashboard"
        self.setupCountryPicker()
        self.serviceRequestForCoronaCaseByCountryd(countryName: "India")
        self.viewSetup()
    }
    
    
    fileprivate func viewSetup(){
        self.selectCountryView.layer.cornerRadius = 5.0
        self.overallStatsView.layer.cornerRadius = 5.0
        self.todayStatsView.layer.cornerRadius = 5.0
        self.totalCaseMainView.layer.cornerRadius = self.totalCaseMainView.layer.frame.width / 2
        
        self.totalCaseMainView.layer.borderWidth = 5
        self.totalCaseMainView.layer.borderColor = UIColor.systemPink.cgColor
    }
    fileprivate func setupCountryPicker(){
        picker.delegate = self
        
        picker.showCallingCodes = true
        /// The nav bar title to show on picker view
        picker.pickerTitle = "Select Your Country"
        
        picker.forceDefaultCountryCode = false
        picker.alphabetScrollBarTintColor = UIColor.black
        
        /// The background color of the alphabet scrollar. Default to clear color
        picker.alphabetScrollBarBackgroundColor = UIColor.systemPink
        
        /// The tint color of the close icon in presented pickers. Defaults to black
        picker.closeButtonTintColor = UIColor.black
        
        picker.font = UIFont(name: "Helvetica Neue", size: 15)
        
        /// The height of the flags shown.  to 40px
        picker.flagHeight = 40
        
        /// Flag to indicate if the navigation bar should be hidden when search becomes active. Defaults to true
        
        picker.searchBarBackgroundColor = UIColor.lightGray
    }
    
    @IBAction func onClickSelectCountry(_ sender: UIButton) {
        self.getCountryPicker()
    }
    
    func getCountryPicker(){
        let pickerNavigationController = UINavigationController(rootViewController: picker)
        self.present(pickerNavigationController, animated: true, completion: nil)
    }
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        self.serviceRequestForCoronaCaseByCountryd(countryName: name)
        self.picker.dismiss(animated: true, completion: nil)
        self.countryCodeLabel.text = code
    }
    
}

extension CountryViewController {
    
    
    fileprivate func serviceRequestForCoronaCaseByCountryd(countryName : String? = "India"){
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://corona.lmao.ninja/countries?sort=count")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error as Any)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse as Any)
                let responseData = JSON(data!)
                print(responseData)
                do {
                    let newJSONDecoder = JSONDecoder()
                    self.countryByData = try! newJSONDecoder.decode(CCData.self, from: data!)
                    self.setupCountryData(countryName: countryName ?? "India")
                }
            }
        })
        
        dataTask.resume()
    }
    fileprivate func setupCountryData(countryName : String){
        
        
        var selectedCountry : CCDatum
        if let contryDataa = self.countryByData {
            for singleData in contryDataa {
                if singleData.country == countryName{
                    selectedCountry = singleData
                    DispatchQueue.main.sync {
                        self.totalCaseMainLabel.text = "\(selectedCountry.cases)"
                        self.overAllDeathLabel.text = "\(selectedCountry.deaths)"
                        self.overallTotalCaseLabel.text = "\(selectedCountry.cases)"
                        self.overallRecoveredCaseLabel.text = "\(selectedCountry.recovered)"
                        self.overAllActiveCaseLabel.text = "\(selectedCountry.active)"
                        
                        self.todayTotalCase.text = "\(selectedCountry.todayCases)"
                        self.todayDeathlabel.text = "\(selectedCountry.todayDeaths)"
                        self.deathPerMillionLabel.text = "\(selectedCountry.deathsPerOneMillion ?? 0.0)"
                        self.casePerMillionLabel.text = "\(selectedCountry.casesPerOneMillion ?? 0.0)"
                        return
                    }
                }
            }
        }
    }
}

