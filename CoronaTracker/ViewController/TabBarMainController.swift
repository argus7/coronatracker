//
//  TabBarMainController.swift
//  CoronaTracker
//
//  Created by Ravi Ranjan on 03/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import UIKit

class TabBarMainController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
        self.navigationItem.leftBarButtonItem = backButton
        self.title  = "Dashboard"
        // Do any additional setup after loading the view.
    }
}
