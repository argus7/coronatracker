//
//  StateViewController.swift
//  CoronaTracker
//
//  Created by Ravi Ranjan on 01/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import UIKit


class StateViewController: UIViewController {
    
    var stateArray = ["Maharashtra","Tamil Nadu","Delhi","Kerala","Uttar Pradesh","Andhra Pradesh","Rajasthan","Telangana","karnataka","Madhya Pradesh","gujarat","jammu and Kashmir","haryana","west Bengal","punjab","bihar","chandigarh","assam","ladakh","Andaman and Nicobar Islands","uttarakhand","chhattisgarh","goa","himachal Pradesh","odisha","puducherry","jharkhand","manipur","mizoram","arunachal Pradesh","Dadra and Nagar Haveli","Daman and Diu","lakshadweep","meghalaya","nagaland","sikkim","tripura"]
    
    
    public var coronaTotalData : CoronaResonseData?
    @IBOutlet weak var stateTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "States Info"
        self.setupStateTableview()

        
    }
    
    fileprivate func setupStateTableview(){
        self.stateTableView.register(UINib(nibName: "stateListCell", bundle: Bundle.main), forCellReuseIdentifier: "stateListCell")
        self.stateTableView.dataSource = self
        self.stateTableView.delegate = self
        
    }
    override func viewWillAppear(_ animated: Bool) {

    }
}

//MARK:- STATE TABLE VIEW DATA SOURCE -
extension StateViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stateArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let stateCell = tableView.dequeueReusableCell(withIdentifier: "stateListCell", for: indexPath) as! stateListCell
        
        stateCell.stateNameLabel.text = self.stateArray[indexPath.row].capitalizingFirstLetter()
        
        var stateData : AndamanAndNicobarIslands?
        switch StateListArra.init(rawValue: self.stateArray[indexPath.row].capitalizingFirstLetter()) {
            
            case .maharashtra:
                stateData = self.coronaTotalData?.stateWise.maharashtra
                break
            case .tamilNadu:
                stateData = self.coronaTotalData?.stateWise.tamilNadu
                break
            case .delhi:
                stateData = self.coronaTotalData?.stateWise.delhi
                break
            case .kerala:
                stateData = self.coronaTotalData?.stateWise.kerala
                break
            case .uttarPradesh:
                stateData = self.coronaTotalData?.stateWise.uttarPradesh
                break
            case .andhraPradesh:
                stateData = self.coronaTotalData?.stateWise.andhraPradesh
                break
            case .rajasthan:
                stateData = self.coronaTotalData?.stateWise.rajasthan
                break
            case .telangana:
                stateData = self.coronaTotalData?.stateWise.telangana
                break
            case .karnataka:
                stateData = self.coronaTotalData?.stateWise.karnataka
                break
            case .madhyaPradesh:
                stateData = self.coronaTotalData?.stateWise.madhyaPradesh
                break
            case .gujarat:
                stateData = self.coronaTotalData?.stateWise.gujarat
                break
            case .jammuAndKashmir:
                stateData = self.coronaTotalData?.stateWise.jammuAndKashmir
                break
            case .haryana:
                stateData = self.coronaTotalData?.stateWise.haryana
                break
            case .westBengal:
                stateData = self.coronaTotalData?.stateWise.westBengal
                break
            case .punjab:
                stateData = self.coronaTotalData?.stateWise.punjab
                break
            case .bihar:
                stateData = self.coronaTotalData?.stateWise.bihar
                break
            case .chandigarh:
                stateData = self.coronaTotalData?.stateWise.chandigarh
                break
            case .assam:
                stateData = self.coronaTotalData?.stateWise.assam
                break
            case .ladakh:
                stateData = self.coronaTotalData?.stateWise.ladakh
                break
            case .andamanAndNicobarIslands:
                stateData = self.coronaTotalData?.stateWise.andamanAndNicobarIslands
                break
            case .uttarakhand:
                stateData = self.coronaTotalData?.stateWise.uttarakhand
                break
            case .chhattisgarh:
                stateData = self.coronaTotalData?.stateWise.chhattisgarh
                break
            case .goa:
                stateData = self.coronaTotalData?.stateWise.goa
                break
            case .himachalPradesh:
                stateData = self.coronaTotalData?.stateWise.himachalPradesh
                break
            case .odisha:
                stateData = self.coronaTotalData?.stateWise.odisha
                break
            case .puducherry:
                stateData = self.coronaTotalData?.stateWise.puducherry
                break
            case .jharkhand:
                stateData = self.coronaTotalData?.stateWise.jharkhand
                break
            case .manipur:
                stateData = self.coronaTotalData?.stateWise.manipur
                break
            case .mizoram:
                stateData = self.coronaTotalData?.stateWise.mizoram
                break
            case .arunachalPradesh:
                stateData = self.coronaTotalData?.stateWise.arunachalPradesh
                break
            case .dadraAndNagarHaveli:
                stateData = self.coronaTotalData?.stateWise.dadraAndNagarHaveli
                break
            case .damanAndDiu:
                stateData = self.coronaTotalData?.stateWise.damanAndDiu
                break
            case .lakshadweep:
                stateData = self.coronaTotalData?.stateWise.lakshadweep
                break
            case .meghalaya:
                stateData = self.coronaTotalData?.stateWise.meghalaya
                break
            case .nagaland:
                stateData = self.coronaTotalData?.stateWise.nagaland
                break
            case .sikkim:
                stateData = self.coronaTotalData?.stateWise.sikkim
                break
            case .tripura:
                stateData = self.coronaTotalData?.stateWise.tripura
                break
            case .unknown:
                print("e")
                break
            case .none:
            print("error")
            break
        }
        stateCell.deathLabel.text = stateData?.deaths
        stateCell.totalLabel.text = stateData?.confirmed
        stateCell.recoveredlabel.text = stateData?.recovered


        return stateCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
}

enum StateListArra : String {
    case maharashtra = "Maharashtra"
    case tamilNadu = "Tamil Nadu"
    case delhi = "Delhi"
    case kerala = "Kerala"
    case uttarPradesh = "Uttar Pradesh"
    case andhraPradesh = "Andhra Pradesh"
    case rajasthan = "Rajasthan"
    case telangana = "Telangana"
    case karnataka = "Karnataka"
    case madhyaPradesh = "Madhya Pradesh"
    case gujarat = "Gujarat"
    case jammuAndKashmir = "Jammu and Kashmir"
    case haryana = "Haryana"
    case westBengal = "West Bengal"
    case punjab = "Punjab"
    case bihar = "Bihar"
    case chandigarh = "Chandigarh"
    case assam = "Assam"
    case ladakh = "Ladakh"
    case andamanAndNicobarIslands = "Andaman and Nicobar Islands"
    case uttarakhand = "Uttarakhand"
    case chhattisgarh = "Chhattisgarh"
    case goa = "Goa"
    case himachalPradesh = "Himachal Pradesh"
    case odisha = "Odisha"
    case puducherry = "Puducherry"
    case jharkhand = "Jharkhand"
    case manipur = "Manipur"
    case mizoram = "Mizoram"
    case arunachalPradesh = "Arunachal Pradesh"
    case dadraAndNagarHaveli = "Dadra and Nagar Haveli"
    case damanAndDiu = "Daman and Diu"
    case lakshadweep = "Lakshadweep"
    case meghalaya = "Meghalaya"
    case nagaland = "Nagaland"
    case sikkim = "Sikkim"
    case tripura = "Tripura"
    case unknown = "Unknown"
}
extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
