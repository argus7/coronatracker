//
//  HomeViewController.swift
//  CoronaTracker
//
//  Created by Ravi Ranjan on 01/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import UIKit
import SwiftyJSON
class HomeViewController: UIViewController {
    
    @IBOutlet weak var todayDeathLabel: UILabel!
    @IBOutlet weak var todayActiveCaseLabel: UILabel!
    @IBOutlet weak var todayRecoveredCaselabel: UILabel!
    @IBOutlet weak var todayTotalCaseLabel: UILabel!
    @IBOutlet weak var totalCaseLabel: UILabel!
    
    @IBOutlet weak var recoveredCaseLabel: UILabel!
    
    @IBOutlet weak var criticalCaselabel: UILabel!
    
    @IBOutlet weak var deathCaseLabel: UILabel!
    
    @IBOutlet weak var numberOfCasesLabel: UILabel!
    @IBOutlet weak var totalConfirmedCaseView: UIView!
    @IBOutlet weak var infoByallCountryView: UIView!
    @IBOutlet weak var infoBystatesView: UIView!
    @IBOutlet weak var todayStatsView: UIView!
    @IBOutlet weak var overallStatsView: UIView!
    @IBOutlet weak var tottalConfirmedCaseLabel: UILabel!
    @IBOutlet weak var totalMainContainerView: UIView!
    //    var homeresponseData : IndiaCovidResult?
    var responseData : CoronaResonseData?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.totalMainContainerView.layer.cornerRadius = 10
        self.totalConfirmedCaseView.layer.cornerRadius = self.totalConfirmedCaseView.frame.width / 2
        self.totalConfirmedCaseView.layer.borderWidth = 5
        self.infoBystatesView.layer.cornerRadius = 5
        self.todayStatsView.layer.cornerRadius = 5
        self.totalConfirmedCaseView.layer.borderColor = UIColor.systemPink.cgColor
        self.totalConfirmedCaseView.layer.borderWidth = 5

        self.overallStatsView.layer.cornerRadius = 5.0
        self.infoByallCountryView.layer.cornerRadius = 5.0
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.service()
    }

    @IBAction func onClickCountryButton(_ sender: UIButton) {
        let countryScreen = self.storyboard?.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
               
               self.navigationController?.pushViewController(countryScreen, animated: true)
    }
    
    @IBAction func onClickViewByStateButton(_ sender: UIButton) {
        
        let stateScreen = self.storyboard?.instantiateViewController(withIdentifier: "StateViewController") as! StateViewController
        
        stateScreen.coronaTotalData = self.responseData
        self.navigationController?.pushViewController(stateScreen, animated: true)
    }
    fileprivate func service(){
        
        
        let headers = [
            "x-rapidapi-host": "corona-virus-world-and-india-data.p.rapidapi.com",
            "x-rapidapi-key": "8b0f1bd79fmshd3be15ac9e63ee2p1b4602jsn5d55892708eb"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://corona-virus-world-and-india-data.p.rapidapi.com/api_india")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error as Any)
            } else {
                
                let resJSON = JSON(data!)
                print(resJSON)
                
                do {
                    let jsonDecoder = JSONDecoder()
                    self.responseData = try jsonDecoder.decode(CoronaResonseData.self, from: data!)
                    self.setupdata()
                    
                } catch {
                    print("Unexpected error: \(error).")
                }
                
                //                self.homeresponseData = IndiaCovidResult(fromJson: jsonData)
            }
        })
        
        dataTask.resume()
        
    }
    fileprivate func setupdata(){
        
        
        DispatchQueue.main.async {
            self.numberOfCasesLabel.text = self.responseData?.totalValues.confirmed
            self.totalCaseLabel.text = self.responseData?.totalValues.confirmed ?? ""
            self.recoveredCaseLabel.text = self.responseData?.totalValues.recovered ?? ""
            self.criticalCaselabel.text = self.responseData?.totalValues.active ?? ""
            self.deathCaseLabel.text = self.responseData?.totalValues.deaths ?? ""
            
            //Today Stats
            
            self.todayTotalCaseLabel.text = self.responseData?.keyValues[0].counterforautotimeupdate ?? ""
            self.todayRecoveredCaselabel.text = self.responseData?.keyValues[0].recovereddelta ?? ""
            self.todayActiveCaseLabel.text = self.responseData?.keyValues[0].confirmeddelta ?? ""
            self.todayDeathLabel.text = self.responseData?.keyValues[0].deceaseddelta ?? ""
        }
        
    }
    
}


/*
 

 extension HomeViewController {
     
     
     fileprivate func serviceRequestForCoronaCaseByCountryd(countryName : String? = "India"){
         
         let request = NSMutableURLRequest(url: NSURL(string: "https://corona.lmao.ninja/countries?sort=count")! as URL,
                                           cachePolicy: .useProtocolCachePolicy,
                                           timeoutInterval: 10.0)
         request.httpMethod = "GET"
         
         let session = URLSession.shared
         let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
             if (error != nil) {
                 print(error as Any)
             } else {
                 let httpResponse = response as? HTTPURLResponse
                 print(httpResponse as Any)
                 let responseData = JSON(data!)
                 print(responseData)
                 do {
                     let newJSONDecoder = JSONDecoder()
                     self.countryByData = try! newJSONDecoder.decode(CCData.self, from: data!)
                     self.setupCountryData(countryName: countryName ?? "India")
                 }
             }
         })
         
         dataTask.resume()
     }
     fileprivate func setupCountryData(countryName : String){
         
         
         var selectedCountry : CCDatum
         if let contryDataa = self.countryByData {
             for singleData in contryDataa {
                 if singleData.country == countryName{
                     selectedCountry = singleData
                     DispatchQueue.main.sync {
    
                         self.numberOfCasesLabel.text = "\(selectedCountry.cases)"
                         self.deathCaseLabel.text = "\(selectedCountry.deaths)"
                         self.totalCaseLabel.text = "\(selectedCountry.cases)"
                         self.recoveredCaseLabel.text = "\(selectedCountry.recovered)"
                         self.criticalCaselabel.text = "\(selectedCountry.active)"
                         
                         
                         
                         self.todayTotalCaseLabel.text = "\(selectedCountry.todayCases)"
                         self.todayDeathLabel.text = "\(selectedCountry.todayDeaths)"

                         return
                     }
                 }
             }
         }
     }
 }


 */
