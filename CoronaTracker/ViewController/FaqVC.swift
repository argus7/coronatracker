//
//  FaqVC.swift
//  CoronaTracker
//
//  Created by Ravi Ranjan on 04/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import UIKit
import SwiftyJSON

class FaqVC: UIViewController {
    
    @IBOutlet weak var faqTable: UITableView!
    var faqJson : QuestionRootClass?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.readJson()
    }
    
    private func readJson() {
        do {
            if let file = Bundle.main.url(forResource: "QuestionAnswerJSON", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSON(data: data)
                self.faqJson = QuestionRootClass(fromJson: json)
                self.faqTable.reloadData()
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    
}
extension FaqVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.faqJson?.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let faqCell = tableView.dequeueReusableCell(withIdentifier: "FaqCell", for: indexPath) as! FaqCell
        faqCell.questionLabel.text = self.faqJson?.data?[indexPath.row].question ?? "Question"
        faqCell.answerLabel.text = self.faqJson?.data?[indexPath.row].answer ?? "Answer"
        return faqCell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
