//
//  SplashVC.swift
//  CoronaTracker
//
//  Created by Ravi Ranjan on 03/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.pushToMainView()
        }
        
    }
    func pushToMainView(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarMainController") as! TabBarMainController
        self.navigationController?.pushViewController(homeVC, animated: false)
    }
    

}
