//
//  BookMarkVC.swift
//  CoronaTracker
//
//  Created by Ravi Ranjan on 04/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import UIKit

class BookMarkVC: UIViewController {
    
    
    let stateArray = ["Maharashtra","Tamil Nadu","Delhi","Kerala","Uttar Pradesh","Andhra Pradesh","Rajasthan","Telangana","karnataka","Madhya Pradesh","gujarat","jammu and Kashmir","haryana","west Bengal","punjab","bihar","chandigarh","assam","ladakh","Andaman and Nicobar Islands","uttarakhand","chhattisgarh","goa","himachal Pradesh","odisha","puducherry","jharkhand","manipur","mizoram","arunachal Pradesh","Dadra and Nagar Haveli","Daman and Diu","lakshadweep","meghalaya","nagaland","sikkim","tripura"]
    
    let mobileNumberArray = ["02026127394","04429510500","01122307145","04712552056","18001805145","08662410978","01412225624","104","104","104","104","01912520982","8558893911","1800313444222","104","104","9779558282","6913347770","01982256462","03192-232102","104","104","104","104","9439994859","104","104","3852411668","102","9436055743","104","104","104","108","7005539653","104","03812315879"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickCentralMobileNumber(_ sender: UIButton) {
        self.callFromApp(mobileNumber: "+911123978046")
    }
}
extension BookMarkVC : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stateArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let stateHelplineCell = tableView.dequeueReusableCell(withIdentifier: "StateHelplineCell", for: indexPath) as! StateHelplineCell
        stateHelplineCell.stateNameLabel.text = stateArray[indexPath.row].capitalizingFirstLetter()
        stateHelplineCell.mobileNumberLabel.text = mobileNumberArray[indexPath.row]
        
        return stateHelplineCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mobileNumber = self.mobileNumberArray[indexPath.row]
        self.callFromApp(mobileNumber: mobileNumber)

    }
    
    fileprivate func callFromApp(mobileNumber : String){
        if let url = URL(string: "tel://\(mobileNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
