//
//  FaqModel.swift
//  CoronaTracker
//
//  Created by Ravi Ranjan on 04/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import Foundation
import SwiftyJSON


struct QuestionRootClass{

    var data : [QuestionDatum]?

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        data = [QuestionDatum]()
        let dataArray = json["data"].arrayValue
        for dataJson in dataArray{
            let value = QuestionDatum(fromJson: dataJson)
            data?.append(value)
        }
    }

}


struct QuestionDatum{

    var answer : String?
    var question : String?

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        answer = json["answer"].stringValue
        question = json["question"].stringValue
    }

}
