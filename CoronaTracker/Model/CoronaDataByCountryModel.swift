//
//  CoronaDataByCountryModel.swift
//  CoronaTracker
//
//  Created by Ravi Ranjan on 03/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import Foundation

struct CountryDatum: Codable {
    let country: String
    let confirmed, recovered, critical, deaths: Int
    let latitude, longitude: Double
}

typealias CountryData = [CountryDatum]




// MARK: - CCDatum
struct CCDatum: Codable {
    let country: String
    let countryInfo: CountryInfo
    let cases, todayCases, deaths, todayDeaths: Int
    let recovered, active, critical: Int
    let casesPerOneMillion, deathsPerOneMillion: Double?
    let updated: Int
}

// MARK: - CountryInfo
struct CountryInfo: Codable {
    let id: Int?
    let iso2, iso3: String?
    let lat, long: Double
    let flag: String

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case iso2, iso3, lat, long, flag
    }
}

typealias CCData = [CCDatum]
