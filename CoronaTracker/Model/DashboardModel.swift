//
//  DashboardModel.swift
//  CoronaTracker
//
//  Created by Ravi Ranjan on 01/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//


import Foundation

// MARK: - Welcome
struct CoronaResonseData: Codable {
    let keyValues: [KeyValue]
    let totalValues: TotalValues
    let stateWise: StateWise

    enum CodingKeys: String, CodingKey {
        case keyValues = "key_values"
        case totalValues = "total_values"
        case stateWise = "state_wise"
    }
}

// MARK: - KeyValue
struct KeyValue: Codable {
    let confirmeddelta, counterforautotimeupdate, deceaseddelta, lastupdatedtime: String
    let recovereddelta, statesdelta: String
}

// MARK: - StateWise
struct StateWise: Codable {
    let maharashtra, tamilNadu, delhi, kerala: AndamanAndNicobarIslands
    let uttarPradesh, andhraPradesh, rajasthan, telangana: AndamanAndNicobarIslands
    let karnataka, madhyaPradesh, gujarat, jammuAndKashmir: AndamanAndNicobarIslands
    let haryana, westBengal, punjab, bihar: AndamanAndNicobarIslands
    let chandigarh: AndamanAndNicobarIslands
    let assam, ladakh, andamanAndNicobarIslands, uttarakhand: AndamanAndNicobarIslands
    let chhattisgarh: AndamanAndNicobarIslands
    let goa, himachalPradesh, odisha, puducherry: AndamanAndNicobarIslands
    let jharkhand, manipur, mizoram, arunachalPradesh: AndamanAndNicobarIslands
    let dadraAndNagarHaveli, damanAndDiu, lakshadweep, meghalaya: AndamanAndNicobarIslands
    let nagaland, sikkim: AndamanAndNicobarIslands
    let tripura: AndamanAndNicobarIslands

    enum CodingKeys: String, CodingKey {
        case maharashtra = "Maharashtra"
        case tamilNadu = "Tamil Nadu"
        case delhi = "Delhi"
        case kerala = "Kerala"
        case uttarPradesh = "Uttar Pradesh"
        case andhraPradesh = "Andhra Pradesh"
        case rajasthan = "Rajasthan"
        case telangana = "Telangana"
        case karnataka = "Karnataka"
        case madhyaPradesh = "Madhya Pradesh"
        case gujarat = "Gujarat"
        case jammuAndKashmir = "Jammu and Kashmir"
        case haryana = "Haryana"
        case westBengal = "West Bengal"
        case punjab = "Punjab"
        case bihar = "Bihar"
        case chandigarh = "Chandigarh"
        case assam = "Assam"
        case ladakh = "Ladakh"
        case andamanAndNicobarIslands = "Andaman and Nicobar Islands"
        case uttarakhand = "Uttarakhand"
        case chhattisgarh = "Chhattisgarh"
        case goa = "Goa"
        case himachalPradesh = "Himachal Pradesh"
        case odisha = "Odisha"
        case puducherry = "Puducherry"
        case jharkhand = "Jharkhand"
        case manipur = "Manipur"
        case mizoram = "Mizoram"
        case arunachalPradesh = "Arunachal Pradesh"
        case dadraAndNagarHaveli = "Dadra and Nagar Haveli"
        case damanAndDiu = "Daman and Diu"
        case lakshadweep = "Lakshadweep"
        case meghalaya = "Meghalaya"
        case nagaland = "Nagaland"
        case sikkim = "Sikkim"
        case tripura = "Tripura"
    }
}

// MARK: - AndamanAndNicobarIslands
struct AndamanAndNicobarIslands: Codable {
    let active, confirmed, deaths: String
    let delta: AndamanAndNicobarIslandsDelta
    let deltaconfirmed, deltadeaths, deltarecovered, lastupdatedtime: String
    let recovered, state, statecode: String
    let district: [String: Bilaspur]?
}

// MARK: - AndamanAndNicobarIslandsDelta
struct AndamanAndNicobarIslandsDelta: Codable {
    let active, confirmed, deaths, recovered: Int
}

// MARK: - Bilaspur
struct Bilaspur: Codable {
    let confirmed: Int
    let lastupdatedtime: String
    let delta: BilaspurDelta
}

// MARK: - BilaspurDelta
struct BilaspurDelta: Codable {
    let confirmed: Int
}

// MARK: - Chandigarh
struct Chandigarh: Codable {
    let active, confirmed, deaths: String
    let delta: AndamanAndNicobarIslandsDelta
    let deltaconfirmed, deltadeaths, deltarecovered, lastupdatedtime: String
    let recovered, state, statecode: String
    let district: ChandigarhDistrict?
}

// MARK: - ChandigarhDistrict
struct ChandigarhDistrict: Codable {
    let chandigarh, unknown: Bilaspur

    enum CodingKeys: String, CodingKey {
        case chandigarh = "Chandigarh"
        case unknown = "Unknown"
    }
}

// MARK: - TotalValues
struct TotalValues: Codable {
    let active, confirmed, deaths: String
    let delta: AndamanAndNicobarIslandsDelta
    let deltaconfirmed, deltadeaths, deltarecovered, lastupdatedtime: String
    let recovered, state, statecode: String
    let district: TotalValuesDistrict?
}

// MARK: - TotalValuesDistrict
struct TotalValuesDistrict: Codable {
    let raipur, rajnandgaon, bilaspur, durg: Bilaspur
    let korba: Bilaspur

    enum CodingKeys: String, CodingKey {
        case raipur = "Raipur"
        case rajnandgaon = "Rajnandgaon"
        case bilaspur = "Bilaspur"
        case durg = "Durg"
        case korba = "Korba"
    }
}

// MARK: - Unknown
struct Unknown: Codable {
    let district: UnknownDistrict
}

// MARK: - UnknownDistrict
struct UnknownDistrict: Codable {
    let unknown: Bilaspur

    enum CodingKeys: String, CodingKey {
        case unknown = "Unknown"
    }
}
