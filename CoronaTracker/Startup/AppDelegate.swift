//
//  AppDelegate.swift
//  CoronaTracker
//
//  Created by Ravi Ranjan on 01/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        UINavigationBar.appearance().customNavigationBar()
        // Override point for customization after application launch.
        return true
    }
}

extension UINavigationBar {
    func customNavigationBar() {
        // color for button images, indicators and etc.

        // color for background of navigation bar
        // but if you use larget titles, then in viewDidLoad must write
        // navigationController?.view.backgroundColor = // your color
        self.tintColor = UIColor.white
        self.barTintColor = .white
        self.isTranslucent = false

        // for larget titles
        self.prefersLargeTitles = true

        // color for large title label
        self.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

        // color for standard title label
        self.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

        // remove bottom line/shadow
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        
    }
}
